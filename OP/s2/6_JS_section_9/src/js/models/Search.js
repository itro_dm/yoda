import axios from 'axios';
import { key, proxy} from '../config';

// function dummy output to console
function dummy_axios_search(url){
    console.log(`Called dummy_axios_search with url=${url}`);
    return {data: {recipes: []}};
}

export default class Search {
    constructor(query) {
        this.query = query;
    }
    async getResults() {
        try {
            const res = await axios(`${proxy}https://www.food2fork.com/api/search?key=${key}&q=${this.query}`);
            //const res = dummy_axios_search(`${proxy}https://www.food2fork.com/api/search?key=${key}&q=${this.query}`);
            
            this.result = res.data.recipes;
        } catch (error) {
            alert(error);
        }    
    }
}