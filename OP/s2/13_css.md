# Адаптивная вёрстка
> 1. Проблемы современной верстки

> 2. Примеры адаптивной верстки

> 3. Что поможет сделать верстку адаптивной

### Адаптивная верстка
> - фиксированные размеры и компоновка

### Отзывчивая версткаверстка
> - относительные размеры и компоновка

## Главное - контент и базовые функции сайта!

### viewport - по умолчанию на мобильных 900 - 1000px (видимая область браузера)
```
<meta name="viewport" content="width=device-width" />
```
width - ширина viewport

device-width - оптимальная ширина для данного устройства

initial-scale - первоначальное приближение

user-scalable=no - можно ли вообще зумить

```
<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1"/>
```
тянись по ширине контейнера, но не больше 1200px;
```
.section__content {
    max-width: 1200px;
}
```
At media
```
@media screen and (max-width: 400px) {
    .header__title {
        font-size: 36px;
    }
}
```
## Спецификация flexbox
http://jonibologna.com/flexbox-cheatsheet/

http://scotch.io/tutorials/a-visual-quide-to-css-flexbox-properties


## CSS Grid
> - Track
> - Row
> - Column
> - Cell
> - Grid line
> - Grid area
> - Gutter (gaps) - расстояние мнжду ячейками

```
.layout_explicit {
    display: grid;
    grid-template-columns: repeat(12, 1fr);
    grid-template-rows: 120px 1fr 120px;
}
.layout_explicit > header {
    grid-column: 1 / 13;    
}
.layout_explicit > main {
    grid-column: 1 / 10;
}
.layout_explicit > section {
    grid-column: 10 / 13;
}
.layout_explicit > footer {
    grid-column: 1 / 13;
}
```
```
grid-template-columns: repeat(auto-fill, 50px);
```
плотная упаковка
```
grid-auto-flow: dense;
```
### Grid умеет
> - выравнивание контейнера и потомков
> - автоматический расчет размера ячеек
> - именованные grid lines

rem - берет начальное значение от HTML контейнера

https://scrimba.com/g/gR8PTE

https://gridbyexample.com

https://drafts.csswg.org/css-grid

https://stackoverflow.com/a/36114937/297939

