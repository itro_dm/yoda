# Цвета и фон. Transition. Animation

## background
```
body {
    background-image: url(http://img.yandex.net/i/www/logo.png);
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: 50% 50%;
    background-color: #CCC;
    background-clip: border-box;
}
```
```
lianer-gradient(60deg, rgba(255,255,255) .3%)
```
### предустаговленные значения
```
contain == auto 100%;
cover == 100% auto;
```
### с фоном можно создавать весьма сложные решения
> http://lea.verou.me/css3patterns/

## transition
> свойства (цве, позиционирование и т.д.) изменяются не резко а плавно (контролируемо)
```
div {
    width: 200px;
    height: 200px;
    background-color: red;
    transition: background-color 0.3s linear;
}
```
```
div {
    left: 0;
    position: relative;
    -moz-transition: left 5s cubic-bezier(.54,.19,.52,.94);
    -moz-transition-delay: 0, 0.5s;
}
```
### transform 
### transform-origin
> - любой блочный елемент можно трансформировать, сщздает новый контекст для z-index, в потоке елемент остается той же формы

## Animation
```
@keyframes my-custom-name {
    from { margin-left: 0; }
    80% { margin-left: 100px; }
    90% { margin-left: 110px; }
    to { margin-left: 0; }
}
```
```
div { 
    animation: 1.75s my-custom-name;
    /* вызов анимации по имени */
    animation-name: my-custom-name;
    /* продолжительность */
    animation-duration: 1.75s;
    /* количество итераций */
    animation-iteration-count: infinite;
    /* анимировать ли в другую сторону */
    animation-direction: reverse;
}
```

## 3D Transform
> - perspective
> - perspective-origin x
> - perspective-origin y

появляется ось z - она смотрит на нас.
