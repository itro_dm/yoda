# CSS - основы модели визуального форматирования
```
{
    width: auto;
    height: auto;
    margin: 0;
    padding: 0;
    border: 0;
}
```

> - Viewport - то что браузер рисует, то что видит пользователь 
> - Containing block
> - Formatting context - поток
> - Block
> - Inline

## позиционирование
> - Normal flow, relative positioning
> - Floats
> - Absolute positioning

## Containing block
> - root (HTML) -> Viewport
> - position: relative/static -> ближайший предок блочный елемент
> - position: absolute -> ближайший предок отличный от static

ширина = width + padding + border
```
елемент будет по центру
{
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    width: 50px;
    height: 50px;
    margin: auto;
    background: red;
}
```

### Border and Padding
> НЕ могут иметь отрицательные значения
### Margin 
> может иметь отрицательное значение

#### высота блочного елемента определяется высотой потомков, если нет padding и border

margin collaps - это схлопывание в нормальном потоке
```
{
margin-top: 5px;
margin-bottom: 10px;
}
будет = 10px;
```
margin collaps - в НЕ нормальном потоке складываются
```
{
float: left;
margin-top: 5px;
margin-bottom: 10px;
}
будет = 15px;
```
### margin-top: 20%; и padding-top: 20%; - считаются от ширины контейнера!

### border - рисуется по верх background
```
border: 20px solid transparent; - прозрачный
```