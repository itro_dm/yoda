# Layouts. At rules

## leyout

### в построении leyout есть несколько задач
> - расположить контент горизонтально
```
display: inline-block;
float: left;
position: absolute;
таблица
```
> - сделать так, чтобы колонки никогда не проваливались одна под пругую
```
white-space: nowrap;
сумма ширин float-блоков всегда равна или меньше ширины родителя
```
> - сделать так чтобы контент не переполнял колонку


## flex-box
> - основная ось - горизонтальная
> - вспомогательная ось - вертикальная
```
flex-direction: column; - переключает оси
flex-direction: row-revers; - переключает направление оси
justity-content: ...; - задает расположение елементов на оси
align-items: ...; - выравнивание по вспомогательной оси
order: ...; - общие свойства отдельному или группе елементов
flex-wrap: ...; - в несколько строк
```

## At rules
> @media... - делается адаптивный дизайн

типы медиа - print и screen и по умолчанию all