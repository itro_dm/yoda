# Layout for mobile devices
> - фиксированный макет (layout)
> - резиновый макет
> - адаптивный макет (прыгает)
> - отзывчивый макет (тянется)

## мобильная версия может хорошо работать в большом вебе!

> - visual viewport - это экран

> - layout viewport - это документ
```
<meta name="viewport" content="width=device-width"> - это документ
```
### Media queries
```
@media [not|only] mediatype [and (media feature)] {
    css-code... ;
}
```
### Media types
> - all - по умолчанию
> - print
> - screen
> - speech - скринридеры

```
@media (min-width:600px) {
    aside {
        если ширина больше 600px приенятся этот CSS;
    }
}
```
(???) --> true --> применится CSS
(???) --> folse --> НЕ применится CSS
```
@media (max-width:600px) {
    aside {
        если ширина меньше 600px приенятся этот CSS;
    }
}
```
### and и , - ставит ограничения ( , = или)
```
@media (min-width:400px) and  (max-width:600px) {
    h1 {
        CSS... ;
    }
}
```
```
@media (max-width:600px), (min-width:400px) {
    h1 {
        CSS... ;
    }
}
```
### Аттрибут media
```
<link rel="stylesheet" media="(max-width: 640px)" href="max-640px.css">

<link rel="stylesheet" media="(min-width: 640px)" href="min-640px.css">

<link rel="stylesheet" media="(orientation: portrait)" href="portrait.css">

<link rel="stylesheet" media="(orientation: landscape)" href="landscape.css">
```

MDN::\<input> - стандарт!

position: fixed; - использовать НЕ надо!
### полезные библиотеки
> - i-ua.js
> - i-scroll5