// /// es5
// 'use strict'
// function test5(bool) { /// var is function scope if var in function we can it even in if statment
//     if(bool) {
//         var varES5 = 'work'
//     }
//     console.log('status: ' + varES5);
// }
// test5(true);

// /// es6

// function test6(bool) { /// let and const have block scope (el in block is when it within {})  
//     let varES6;
//     if(bool) {
//         varES6 = 'work good';
//     }
//     console.log('status: ' + varES6);
// }
// test6(true);



// let i = 23;

// for(let i = 0; i < 5; i++) {
//     console.log(i)
// }

// console.log(i);

// let year = new Date();

// let name = `Dima`;

// function calcAge(yearBirth) {
//     return year.getFullYear() - yearBirth;
// }

// console.log(`i am ${calcAge(2000)} years old`);

// console.log(name.startsWith('D'));
// console.log(name.endsWith('a'));
// console.log(name.includes('im'));
// console.log(`${name} `.repeat(5));




// const years = [1990, 1965, 1982, 1937];

// let ages = years.map((i, index) => `${index}. ${2019 - i}`);

// var box5 = {
//     color: 'green',
//     position: 1,
//     clickMe: function() {
//         var that = this;
//         document.querySelector('.green').addEventListener('click', function() {
//             var str = 'This is box number ' + that.position + ' and it is ' + that.color;
//             alert(str);
//         })
//     }
// }
// box5.clickMe();

// const box6 = {
//     color: 'green',
//     position: 1,
//     clickMe: function() {
        
//         document.querySelector('.green').addEventListener('click', () => {
//             var str = `This is box number ${this.position} and it is ${this.color}`;
//             alert(str);
//         });
//     }
// }
// box6.clickMe();


///es5
// function Person(name) {
//     this.name = name;
// }

// Person.prototype.myFriends = function(friends) {
//     // var that = this;
//     var arr = friends.map(function(el) {
//         return this.name + ' is friends with ' + el;
//     }.bind(this));
//     console.log(arr);

// }

// var friends = ['Bob', 'Jane', 'Mark'];
// new Person('John').myFriends(friends);


///es6 

// function Person(name) {
//     this.name = name;
// }

// Person.prototype.myFriends = function(friends) {
//     // var that = this;
//     let arr = friends.map((el) => {
//         return `${this.name} is friends with ${el}`;
//     });
//     console.log(arr);

// }

// var friends = ['Bob', 'Jane', 'Mark'];
// new Person('John').myFriends(friends);

///es5
// var john = ['John', 26];
// var name = john[0];
// var age = john[1];


// ///es6
// const [name, age] = ['John', 26];
// console.log(`${name}, ${age}`);

// const obj = {
//     firstName: 'John',
//     lastName: 'Smith'
// };

// const {firstName, lastName} = obj;
// console.log(`${firstName}, ${lastName}`);
// const {firstName: a, lastName: b} = obj;
// console.log(`${a}, ${b}`);


// function calcAge(year) {
//     const age = new Date().getFullYear() - year;
//     return [age, 65 - age];
// }


// const [ageNow, retirement] = calcAge(2000);
// console.log(`${ageNow}, ${retirement}`)





// const boxes = document.querySelectorAll('.box');

// ///es5 

// var boxesArr5 = Array.prototype.slice.call(boxes);
// boxes.forEach(function(cur) {
//     cur.style.backgroundColor = 'dodgerblue';

// })


// ///es6 

// const boxesArr6 = Array.from(boxes);
// boxesArr6.forEach(i => {
//     i.style.backgroundColor = 'dodgerblue';
// })


///es5

// for(var i = 0; i < boxesArr5.length; i++) {

//     if(boxesArr5[i].className === 'box blue') {
//         break;
//     }

//     boxesArr5[i].textContent = 'I changed to blue'; 

// }


// ///es6

// for(let i of boxesArr6) {
//     if(i.className.includes('blue')) {
//         continue;
//     }
//     i.textContent = 'I changed to blue'
// }



// ///es5


// var ages = [12, 17, 8, 21, 14, 11];

// var full = ages.map(function(el){
//     return el >= 18;
// });
// console.log(full);

// console.log(full.indexOf(true));
// console.log(ages[full.indexOf(true)]);


// ///es6 

// console.log(ages.findIndex(i =>  i >= 18 ));
// console.log(ages.find(i => i >= 18 ));




// function addFourAges(a, b, c, d) {

//     return a + b + c + d;

// }

// var sum1 = addFourAges(18, 30, 12, 21);
// console.log(sum1);


// ///es5 

// var ages = [18, 30, 12, 21];
// var sum2 = addFourAges.apply(null, ages);
// console.log(sum2);

// //es6

// const sum3 = addFourAges(...ages);
// console.log(sum3);

// const familySmith = ['John', 'Jane', 'Mark'];
// const familyMiller = ['Mary', 'Bob', 'Ann'];
// const bigFamily = [...familySmith, 'Lily', ...familyMiller];
// console.log(bigFamily);



// const h = document.querySelector('h1');
// const boxes = document.querySelectorAll('.box');

// const all = [h, ...boxes];

// Array.from(all).forEach(i => i.style.color = 'purple');


// function isFullAge5() {
//     // console.log(arguments);
//     var argsArr = Array.prototype.slice.call(arguments);
//     argsArr.forEach(function(cur){
//         console.log((2016 - cur) >= 18);
//     })
// }

// // isFullAge5(1990, 1999, 1965);
// // isFullAge5(1990, 1999, 1965, 2016, 1987);


// ///es6 


// function isFullAge6(...years) {
//     years.forEach(cur => {
//         console.log((2016 - cur) >= 18);
//     });
// }

// isFullAge6(1990, 1999, 1965, 2016, 1987);



// ///es5
// function isFullAge5(limit) {
//     // console.log(arguments);
//     var argsArr = Array.prototype.slice.call(arguments, 1);
//     console.log(argsArr);
//     argsArr.forEach(function(cur){
//         console.log((2016 - cur) >= limit);
//     })
// }

// isFullAge5(1990, 1999, 1965);
// // isFullAge5(1990, 1999, 1965, 2016, 1987);


// ///es6 


// function isFullAge6(limit,...years) {
//     years.forEach(cur => {
//         console.log((2016 - cur) >= limit);
//     });
// }

// isFullAge6(22, 1990, 1999, 1965, 2016, 1987);



///es5
// function SmithPerson(firstName, yearOfBirth, lastName, nationality) {

//     lastName === undefined ? lastName = 'Smith' : lastName;
//     nationality === undefined ? nationality = 'american' : nationality;


//     this.firstName = firstName;
//     this.yearOfBirth = yearOfBirth;
//     this.lastName = lastName;
//     this.nationality = nationality;

// }


// let john = new SmithPerson('John', 1990);

// let emily = new SmithPerson('Emily', 1983, 'Diaz', 'spanish');


///es6


// function SmithPerson(firstName, yearOfBirth, lastName = 'Smith', nationality = 'american') {

//     this.firstName = firstName;
//     this.yearOfBirth = yearOfBirth;
//     this.lastName = lastName;
//     this.nationality = nationality;

// }

// let john = new SmithPerson('John', 1990);

// let emily = new SmithPerson('Emily', 1983, 'Diaz', 'spanish');




// const question = new Map();
// question.set('question', 'What is the official name of the lateste major JavaScript version?');
// question.set(1, 'ES5');
// question.set(2, 'ES6');
// question.set(3, 'ES2015');
// question.set(4, 'ES7');
// question.set('correct', 3);
// question.set(true, 'Correct answer');
// question.set(false, 'Wrong, please try again!');

// console.log(question.get('question')); /// получаем доступ к значению
// console.log(question.size) /// узнать размер MAP;




// if(question.has(4)) { /// проверяем есть ли элемент в MAP
//     // question.delete(4); /// удаляем элемент

// }
// // question.clear(); /// удаляем все элемнты с MAP, но не сам MAP


// // question.forEach((value, key) => {
// //     console.log(`This is ${key}, and it is set to ${value}`);
// // });


// for(let [key, value] of question.entries()) { 

//     if(typeof key === 'number') {
//         console.log(`Answer ${key}: ${value}`);
//     }

// }


// const ans = parseInt(prompt('Write the correct answer'));

// console.log(question.get(ans === question.get('correct')));





 

///es5
var Person5 = function(name, yearOfBirth, job) {

    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
}



Person5.prototype.calculateAge = function() {
    var age = new Date().getFullYear() - this.yearOfBirth;
    console.log(age);
}

var Athlete5 = function(name, yearOfBirth, job, olimpicGames, medals) {

    Person5.call(this, name, yearOfBirth, job);
    this.olimpicGames = olimpicGames;
    this.medals = medals;

}



Athlete5.prototype = Object.create(Person5.prototype);

Athlete5.prototype.wonMedals = function() {
    this.medals++;
    console.log(this.medals);
}

var johnAthlete5 = new Athlete5('John', 1990, 'swimmer', 3, 10);

johnAthlete5.calculateAge();
johnAthlete5.wonMedals();


///es6 
class Person6 {
    constructor(name, yearOfBirth, job) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.job = job;
    }
    calculateAge() {
        let age = new Date().getFullYear() - this.yearOfBirth;
        console.log(age);
    }
}

class Athlete6 extends Person6{

        constructor(name, yearOfBirth, job, olimpicGames, medals) {

            super(name, yearOfBirth, job); /// наследуем от супер класса(Person6)
            this.olimpicGames = olimpicGames;
            this.medals = medals;
            
        }

        wonMedal() {
            this.medals++;
            console.log(this.medals);   
        }
}

const johnAthlete6 = new Athlete6('John', 1990, 'swimmer', 3, 10);

johnAthlete6.wonMedal();
johnAthlete6.calculateAge();