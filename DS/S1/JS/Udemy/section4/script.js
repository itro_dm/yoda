/*

var john = {
    name: 'John',
    yearOfBirth: 1990,
    job: 'teacher'
};

var Person = function(name, yearOfBirth, job) {
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
    this.calculateAge = function() {
            this.age = 2019 - this.yearOfBirth;
    }
}

// вместо 11 - 13 можно

// Person.prortotype.calculateAge = function() {
//     this.age = 2019 - this.yearOfBirth;
// } 


Person.prototype.lastName = 'Smith';

var john = new Person('John', 1990, 'teacher');

var jane = new Person('Jane', 1969, 'deigner');

var mark = new Person('Mark', 1948, 'retired');

*/

//Object.create

// var personProto = {
//     calculateAge: function() {
//         console.log(2019 - this.yearOfBirth)
//     }
// }



// var john = Object.create(personProto);

// john.name = 'John';
// john.yearOfBirth = '1990';
// john.job = 'teacher';
// console.log(john);

// var jane = Object.create(personProto, {
//     name: {value: 'Jane'},
//     yearOfBirth: {value: 1969},
//     job: {value: 'designer'}
// });

// console.log(jane);



// var years = [1990, 1965, 1937, 2005, 1998];

// function arrayCalc(arr, fn) {
//     var arrRes = [];
//     for (var i = 0; i < arr.length; i++) {
//         arrRes.push(fn(arr[i]));
//     } 
//     return arrRes;
// }

// function calculateAge(year) {
//     return 2019 - year;
// }

// function isFullAges(el) {
//     return el >= 18;
// }

// function maxHeartRate(el) {
//     if(el >= 18 && el <= 81) {
//         return Math.round( 206.9 - ( 0.67 * el ) );
//     } else {
//         return -1;
//     }
// }

// let arrAge = arrayCalc(years, calculateAge);

// console.log(arrayCalc(arrAge, maxHeartRate));


// function interviewQuestion(job) {
//     if(job === 'designer') {
//         return function(name) {
//             console.log(`${name}, can you please explain what UX design is?`);
//         }
//     } else if(job === 'teacher') {
//         return function(name) {
//             console.log(`What subject do you teach, ${name}?`);
//         }
//     } else {
//         return function() {
//             console.log(`Hallo ${name}, what do you do?`)
//         }
//     }
// }
// // let desQuetion = interviewQuestion('designer');
// // desQuetion('dima');
// interviewQuestion('designer')('dima'); 


// function retirement(retirementAge) {
//     var a = ' years left until retirement.';
//     return function(yearOfBirth) {
//         var age = 2019 - yearOfBirth;
//         console.log((retirementAge - age ) + a);
//     } 

// }

// retirement(65)(2000);



// function interviewQuestion(job) {
//     return function(name) {
//         if(job === 'designer') {
//             console.log(`${name}, can you please explain what UX design is?`);
//         } else if(job === 'teacher') {
//             console.log(`What subject do you teach, ${name}?`);
//         } else {
//             console.log(`Hallo ${name}, what do you do?`);
//         }
//     }
// }

// interviewQuestion('teacher')('Dima');



// bind apply call


var john = {
    name: 'John',
    age: 26,
    job: 'teacher',
    presentation: function(style, timeOfDay) {
        if(style === 'formal') {
            console.log(`Good ${timeOfDay}, Ladies and gentleman! I'm ${this.name}, I'm f ${this.job} and I'm ${this.age} years old`);
        } else if (style === 'friendly') {
            console.log(`Hey! What's up? I'm ${this.name}, I'm f ${this.job} and I'm ${this.age} years old. Have a nice ${timeOfDay}.`);
        }
    }
}

var emily = {
    name: 'Emily',
    age: 35,
    job: 'designer'
}

john.presentation.call(emily, 'formal', 'afternoon')

let johnFriendly = john.presentation.bind(john, 'friendly');
johnFriendly('morning');
johnFriendly('night');





var years = [1990, 1965, 1937, 2005, 1998];

function arrayCalc(arr, fn) {
    var arrRes = [];
    for (var i = 0; i < arr.length; i++) {
        arrRes.push(fn(arr[i]));
    } 
    return arrRes;
}

function calculateAge(year) {
    return 2019 - year;
}

function isFullAges(limit, el) {
    return el >= limit;
}


let age = arrayCalc(years, calculateAge);

let fullJapan = arrayCalc(age, isFullAges.bind(this, 20));

console.log(age);
console.log(fullJapan);


