/// BUDGET CONTROLLER
const budgetController = (function() {
    /// создаем экземпляры объектов затрат и прибыли
    const Expense = function(id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
        this.percentage = -1;
    };

    Expense.prototype.calcPercentage = function(totalIncome) {
        if(totalIncome > 0) {
            this.percentage = Math.round((this.value / totalIncome) * 100);
        } else {
            this.percentage = -1;
        }

    };

    Expense.prototype.getPercentage = function() {
        return this.percentage;
    };

    const Income = function(id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    }

    const calculateTotall = (type) => { /// считаем данные
        let sum = 0;
        data.allItems[type].forEach(i => { /// считаем сумму затрат или прибыли
            sum += i.value;
        })
        data.totals[type] = sum; /// заносим суммму в объект
    };

    let totalExpenses = 0;

    let data = { /// хранение данных об убытке и прибыди 
        allItems: { /// сами элементы
            exp: [],
            inc: []
        },
        totals: { /// количествиные даные 
            exp: 0,
            inc: 0
        },
        budget: 0,
        percentage: -1
    };
    // const getUnicId = (type) => {
    //     if (data.allItems[type].length) {
    //         const arrayOfTypeTransaction = data.allItems[type];
    //         const idOfLastElemnt = arrayOfTypeTransaction[arrayOfTypeTransaction.length -1].id;
    //         return idOfLastElemnt + 1;
    //     } else {
    //         return 0;    
    //     }
    // }


 
    return {
        addItem: function(type, des, val) { /// метод для создания объекта убытоков(exp), или прибыли(inc)
            //////////
            if(data.allItems[type].length > 0) { // создаем id для элемента
                ID = data.allItems[type][data.allItems[type].length - 1].id + 1; 
            } else {
                ID = 0;
            }
             //////
            let newItem;
            if (type === 'exp') { /// создаем объект элемента
                newItem = new Expense(ID, des, val);
            } else if (type === 'inc') {
                newItem = new Income(ID, des, val);
            }
            data.allItems[type].push(newItem);
            return newItem;     /// возращаем объект элемента 
        }, 

        deleteItem: function(type, id) { /// метод для удаление элемнта из бызы данных
            let ids, index;
            ids = data.allItems[type].map(i => { ///создаем массив с id элементов
                return i.id;
            })

            index = ids.indexOf(id); /// записываем индекс элемена 

            if(index !== -1) {
                data.allItems[type].splice(index, 1); /// удаляем элемент из массива данных
            }

        },
        calculateBudget: function() {
            
            /// calculate total income and expenses 
            calculateTotall('exp');
            calculateTotall('inc');

            /// Calculate the budget: income - expenses 
            data.budget = data.totals.inc - data.totals.exp;

            /// calculate the percentage of income that we spent
            if (data.totals.inc > 0) { 
                data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
            } else {
                data.percentage = -1;
            }

            /// 
        },

        calculatePercentages: function() {

            data.allItems.exp.forEach(i => {
                i.calcPercentage(data.totals.inc);
            });

        },
        getPercentages: function() {
            let allPerc = data.allItems.exp.map(i => {
                return i.getPercentage();
            });
            return allPerc;
        },

        getBudget: function() { /// возвращаем даные по финансам
            return {
                budget: data.budget,
                totalInc: data.totals.inc,
                totalExp: data.totals.exp,
                percentage: data.percentage
            };
        }
    }

    

})();

/// UI CONTROLLER
const UIController = (function() {
    /// Создаем объект со всеми селекторами с котороми взаимодействуем 
    let DOMstrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeContainer: '.income__list',
        expensesContainer: '.expenses__list',
        budgetLabel: '.budget__value',
        incomeLabel: '.budget__income--value',
        expensesLabel: '.budget__expenses--value',
        percentageLabel: '.budget__expenses--percentage',
        container: '.container',
        expensesPercLabel: '.item__percentage',
        dateLabel: '.budget__title--month'
    }
    let formatNumber = (num, type) => {
        let numSplit, int, dec;
        num = Math.abs(num);
        num = num.toFixed(2);
        numSplit = num.split('.');  
        int = numSplit[0];

        if(int.length > 3) {
            int = `${int.substr(0, int.length - 3)},${int.substr(int.length - 3, int.length)}`; /// добовляем запятые в тысячные 23500 -> 23,500
        }

        dec = numSplit[1];

        return `${type === 'exp' ? '-' : '+'} ${int}.${dec}`;
    };

    let nodeListForEach = function(list, callback) {
        for(let i = 0; i < list.length; i++) {
            callback(list[i], i);
        }
    };

    return {
        getInput: function() {

            return { //Объеденяем все значение форм в один объект
                type: document.querySelector(DOMstrings.inputType).value, // Получаем значение форм 
                description: document.querySelector(DOMstrings.inputDescription).value,     
                value: parseFloat(document.querySelector(DOMstrings.inputValue).value) /// конвентируем значение поля в number
            }

        },
        changedType: () => {

            let fields = document.querySelectorAll(

                DOMstrings.inputType + ', ' +
                DOMstrings.inputDescription + ', ' +
                DOMstrings.inputValue

            );

            nodeListForEach(fields, i => {

                i.classList.toggle('red-focus');

            });

            document.querySelector(DOMstrings.inputBtn).classList.toggle('red');

        },

        getDOMstrings: function() { /// через этот метод делаю объект со всеми селекторами доступным в других модулях
            return DOMstrings;
        },

        addListItem: function(obj, type) {

            //Create HTML string with placeholder text 
            let html, newHtml, element;
            
            if(type === 'inc'){
                element = DOMstrings.incomeContainer;
                html = '<div class="item clearfix" id="inc-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>'
            } else if(type === 'exp') {
                element = DOMstrings.expensesContainer;    
                html = '<div class="item clearfix" id="exp-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__percentage">21%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>'
            }
            
            //Replace the placeholder text with some actual data 

            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%description%', obj.description);
            newHtml = newHtml.replace('%value%', formatNumber(obj.value, type))

            //Insert the HTML into the DOM 

            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);/// создаем на страничке элементы с прибылью и убытком

        },
        deleteListItem: function(selectorID) { /// метод для удаление элемента с DOM

            let el = document.getElementById(selectorID);
            el.parentNode.removeChild(el);

        },
        clearFields: function() { ///метод для очистки полей 
            let fields, fieldsArr;
            fields = document.querySelectorAll(DOMstrings.inputDescription + ', ' + DOMstrings.inputValue);
            fieldsArr = Array.prototype.slice.call(fields);
            fieldsArr.forEach(i => {
                i.value = '';
            });
            fieldsArr[0].focus(); // после того как элемент создан фокус на поле ввода
        },
        displayPercentages: function(percentages) {
           
            let fields = document.querySelectorAll(DOMstrings.expensesPercLabel);


            nodeListForEach(fields, (i, index) => {

                if(percentages[index] > 0) {
                    i.textContent = `${percentages[index]}%`;
                } else {
                    i.textContent = '';
                }

            });

        },
        displayMonth: () => {
            let now, months, month, year;
            now = new Date();
            months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            month = now.getMonth();
            year = now.getFullYear();
            document.querySelector(DOMstrings.dateLabel).textContent = `${months[month]} ${year}`;

        },
        displayBudget: function(obj) { /// Вывод budget на страничку 
            let type;
            obj.budget > 0 ? type = 'inc' : type = 'exp';

            document.querySelector(DOMstrings.budgetLabel).textContent = formatNumber(obj.budget, type);
            document.querySelector(DOMstrings.incomeLabel).textContent = formatNumber(obj.totalInc, 'inc');
            document.querySelector(DOMstrings.expensesLabel).textContent = formatNumber(obj.totalExp, 'exp');
            
            if(obj.percentage > 0) {
                document.querySelector(DOMstrings.percentageLabel).textContent = `${obj.percentage}%`;
            } else {
                document.querySelector(DOMstrings.percentageLabel).textContent = ``;
            }

        }
    }

})();

///GLOBAL APP CONTROLLER
const controller = (function(budgetCtrl, UICtrl) {

    let updateBudget = function() {

        //1. Calculate the budget 
        budgetCtrl.calculateBudget();
        //2. return the budget

        let budget = budgetCtrl.getBudget(); /// передаем значение с финансами

        //3. Dispaly the budget on the UI controller 
        UICtrl.displayBudget(budget);
    }

    let updatePercentages = function() {

        //1. Calculate percentages 
        budgetCtrl.calculatePercentages();
        //2. Read percentages from the budget controller 
        let percentages = budgetCtrl.getPercentages();
        //3. Update the UI
        UICtrl.displayPercentages(percentages); 

    };

    let setupEventListeners = function() { // Помещаем все события в ону функцию
        
        let DOM = UICtrl.getDOMstrings(); ///сохроняю сюда объект со всеми селекторами
        
        document.querySelector(DOM.inputBtn).addEventListener('click', ctrlAddItem);  

        document.addEventListener('keypress', function(event) {
            
            if (event.keyCode === 13 || event.which === 13) {
                ctrlAddItem();
            }
            
        });

        document.querySelector(DOM.container).addEventListener('click', ctrlDeleteItem);

        document.querySelector(DOM.inputType).addEventListener('change', UICtrl.changedType);

    };


    const ctrlAddItem = function() {

        let input, newItem;
        //1. Get the field input data 
        input = UIController.getInput(); // передаем объект с значениями подей
        if(input.description !== "" && !isNaN(input.value) && input.value > 0) { // !isNaN(input.value) проверяет чтоб в поле всегда было число
            //2. Add the item to the budget controller 
            newItem = budgetCtrl.addItem(input.type, input.description, input.value);
            //3. Add the item to the UI
            UICtrl.addListItem(newItem, input.type);
            //4.Clear fields
            UICtrl.clearFields();
            //5. Calculate and update budget
            updateBudget();
            //6. Calculate and update percentages
            updatePercentages();
        }

    };

    const ctrlDeleteItem = function(event) {
        let itemID;
        itemID = event.target.parentNode.parentNode.parentNode.parentNode.id; // получаем доступ к 4-му родителя элемента на который мы нажали
        if(itemID) {

            // inc-0
            splitID = itemID.split('-'); // разбиваем id на тип(прибыль и убыток) и номер
            type = splitID[0]; 
            ID = parseInt(splitID[1]); /// конвентируем так как indexOf('1'), не поймет если мы передадим в кач-ве аргумента стоку
            
            // 1. delete the item from the data structure 
            budgetCtrl.deleteItem(type, ID);
            // 2. delete the item from the UI
            UICtrl.deleteListItem(itemID);
            // 3. Update the budget
            updateBudget(); 
            // 4. Calculate and update percentages
            updatePercentages();
        }
    }

    return { // делаем события поклику доступными для всех модулей
        init: function() {
            console.log('Application has started.');
            UICtrl.displayMonth();
            UICtrl.displayBudget({ budget: 0, totalExp: 0, totalExp: 0, percentage: -1 });
            setupEventListeners();
        }
    }

})(budgetController, UIController);


controller.init(); /// Делаем чтоб EventListener работал

