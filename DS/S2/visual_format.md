# Containing block 

- root(HTML) -> Viewport
- `position: relative/static` - ближайший блочный элемент
- `position: absolute` - ближайший предок с `position` отличным от `static`

# Margin

- `margin-top: n%` - считается от ***ширины*** контейнера с `padding-top` тоже самое