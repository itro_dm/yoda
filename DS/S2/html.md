# HTML

## Мета-теги

`<meta http-equiv="" content="">`

- `http-equiv` - устанавливает НТТP заголовок
- `content` - значение атрибутов
- `name` - устонавливет модификатор мета-тега

## Viewport

###### Example

`<meta name="viewpor" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=no">`

- `width` - ширинаб,
- `maximum-scale` - максимальный масштаб,
- `initial-scale` - начальный масштаб,
- `user-scalable` - изменение масштаба(yes/no)


## OpenGraph

`<meta property="og:title" content="Попробуйте Яндекс.Диск - новыйсервис...">`

- `og:description` - описание
- `og:image` - первью-картинка
- `og:video` - видео
- `og:url` - путь к странице


## Текстовые теги 

- `<pre>` - сохраняет пробелы и разрывы
- `<hr>` - горизонтальная линия


## Список определений 
```

<dl>
    <dt>HTML</dt>
    <dd>Some text</dd>
</dl>
```

###### Preview

* HTML
    * Some text

## Замещаемы элементы

- `<progress value="30" max="100"></progress>` - создает "прогрес-бар"
- `<canvas width="" height=""></canvas>` - холст на котром мы рисуем с помощью JS


## html-entity

| Символ | Десятичный код | html-код | Описание |
| --- | --- | --- | --- |
| " | &# 34; | & quote; | Двойные  ковычки |
| ' | &# 39; | & apos; | Апостроф |
| & | &# 38; | & amp; | Амперсанд |
| < | &# 60; | & lt; | Меньше |
| > | &# 62; | & gt; | Больше |
|   | &# 160; | & nbsp; | Неразрывный пробел |



