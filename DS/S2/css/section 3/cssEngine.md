# Three pillars of writing good HTML and CSS

1. ***Responsive design***

- Fluid layouts(гибкии макеты)
- Media queries
- Responsive images
- Correct units(единицы измерение)
- Desktop-first vs mobile-first

2. ***Maintainable and scable code***

- Clean
- Easy-to-understand
- Growth(развитие)
- Reusable
- How to organize files 
- How to name classes
- How strucutre HTML

3. ***Web perfomance***

- Less HTTP request
- Less code
- Compress code 
- Use a CSS preprocessore 
- Less images 
- Compress images

# How load page 

```
1. Load HTML -> Parse HTML -----------------> DOM 
                    |                           | 
                    Load CSS -> Parse CSS -> CSSOM

2. DOM
    | -> Render tree -> website rendering the visual formatting model -> final render website
  CSSOM
```

# How CSS is parsed 

Resolve conflicts CSS decloration

- Process of combining different stylesheets and resolving conflicts between different CSS rules and decloration, when than one rule applies to certain element

- all relative units convert to px;


## How realtive units work

- em(fonts) - use font-size of parent
- em(length) - use font-size of current element

## Boxes 

| Block | Inline-block | Inline |
| --- | --- | --- |
| Elements formatted visualy as boxes | A mix of block and inline | Content is distributed in lines |
| 100% of parents width | Ocupies content's space | Ocupies content's space|
| Vertically, one after another | No line-breaks | No line-breaks |
| | | No heights and width |
| | | padding and margin only horizantal |
| `display: block, flex, list-item` | `display: inline-block` | `display: inline` |

## CSS Architecture 

THINK -> BUILD -> ARCHITECT

**THINK**

- Modular building blocks that *make up*(состовляют) interfaces;
- Held together by the layout of the page(Удерживается вместе макет страницы);
- Re-usable across the project, and between differnet project;
- Independent, allowing us to use them anywhere on the page;(Независимо, что позволяет нам использовать их в любом месте на странице)

**BUILD**

BEM 

- Block Element Modifier 
- BLOCK: standalone component that is meaningful on its own(автономный компонент, который имеет смысл сам по себе)
- ELEMENT: part of block that has no standalone meaning(часть блока, которая не имеет самостоятельного значения)
- MODIFIER: a different vesion of a block or an element

**ARCHITEST**

THE 7-1 PATTERN

7 different folders for partial(частичный) SASS files, and 1 main SASS file to imoprt all other files into a complied CSS stylesheets 

THE 7 FOLDERS

- base/
- components/ 
- layout/
- pages/
- themes/
- abstracts/
- vendors/

