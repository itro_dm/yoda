# MAIN SASS FEATURES

- **Variables:** for reusable values such as colors, fint-size, spacing, etc 
- **Nesting:** to nest selectors inside of one another, allowing us to write less code
- **Operators:** for mathematical operations right inside of CSS
- **Partioals and imports:** to write CSS in different files and importing them all into one signle file
- **Mixins:** to write reusable pieces of CSS code 
- **Functions:** similar to mixins, with the differnce that they produce a value that can than be used 
- **Extends:** to make different selectors inherit declarations that are common to all of them
- **Control directives:** for writing complex code using conditionals and loops 